DROP DATABASE IF EXISTS demo_agit;
CREATE DATABASE demo_agit;

DROP DATABASE IF EXISTS public.tbl_buku;
CREATE TABLE public.tbl_buku (
	id bigserial NOT NULL,
	id_buku varchar(255) NULL,
	judul varchar(255) NULL,
	peminjam varchar(255) NULL,
	penerbit varchar(255) NULL,
	pengarang varchar(255) NULL,
	status varchar(255) NULL,
	tebal_buku varchar(255) NULL,
	tgl_kembali varchar(255) NULL,
	tgl_pinjam varchar(255) NULL,
	tgl_terbit varchar(255) NULL,
	CONSTRAINT tbl_buku_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
