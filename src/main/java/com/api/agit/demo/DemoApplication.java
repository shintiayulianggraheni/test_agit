package com.api.agit.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
@EnableTransactionManagement
public class DemoApplication {
	private final static Logger log = LogManager.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		log.info("APPLICATON IS UP");
		SpringApplication.run(DemoApplication.class, args);
	}

}
