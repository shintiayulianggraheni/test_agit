package com.api.agit.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.agit.demo.contract.request.DeleteBookRequest;
import com.api.agit.demo.contract.request.FilterByStatusRequest;
import com.api.agit.demo.contract.request.UpdateBookDataRequest;
import com.api.agit.demo.contract.request.UpdateBookStatusRequest;
import com.api.agit.demo.contract.response.CreateBookResponse;
import com.api.agit.demo.contract.response.DeleteBookResponse;
import com.api.agit.demo.contract.response.UpdateBookDataResponse;
import com.api.agit.demo.contract.response.UpdateBookStatusResponse;
import com.api.agit.demo.model.Book;
import com.api.agit.demo.service.BookService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/book")
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/getAll", method=RequestMethod.GET)
	public List<Book> readAllBooks() {
	    return bookService.getAllBooks();
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/getByStatus", method=RequestMethod.GET)
	public List<Book> readByStatus(@RequestBody FilterByStatusRequest request) {
	    return bookService.getDataByStatus(request);
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public CreateBookResponse create(@RequestBody Book request) {
		CreateBookResponse response = new CreateBookResponse();
		response = bookService.createBook(request);
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public UpdateBookDataResponse update(@RequestBody UpdateBookDataRequest request) {
		UpdateBookDataResponse response = new UpdateBookDataResponse();
		response = bookService.updateDataBook(request);
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/updateStatus", method=RequestMethod.POST)
	public UpdateBookStatusResponse updateStatus(@RequestBody UpdateBookStatusRequest request) {
		UpdateBookStatusResponse response = new UpdateBookStatusResponse();
		response = bookService.updateStatusBook(request);
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public DeleteBookResponse delete(@RequestBody DeleteBookRequest request) {
		DeleteBookResponse response = new DeleteBookResponse();
		response = bookService.delete(request);
		return response;
	}
}
