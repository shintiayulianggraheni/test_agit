package com.api.agit.demo.service;

import java.util.List;

import com.api.agit.demo.contract.request.DeleteBookRequest;
import com.api.agit.demo.contract.request.FilterByStatusRequest;
import com.api.agit.demo.contract.request.UpdateBookDataRequest;
import com.api.agit.demo.contract.request.UpdateBookStatusRequest;
import com.api.agit.demo.contract.response.CreateBookResponse;
import com.api.agit.demo.contract.response.DeleteBookResponse;
import com.api.agit.demo.contract.response.UpdateBookDataResponse;
import com.api.agit.demo.contract.response.UpdateBookStatusResponse;
import com.api.agit.demo.model.Book;

public interface BookService {

	public List<Book> getAllBooks();

	public List<Book> getDataByStatus(FilterByStatusRequest request);

	public CreateBookResponse createBook(Book request);

	public UpdateBookDataResponse updateDataBook(UpdateBookDataRequest request);

	public UpdateBookStatusResponse updateStatusBook(UpdateBookStatusRequest request);

	public DeleteBookResponse delete(DeleteBookRequest request);

}
