package com.api.agit.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.agit.demo.contract.request.DeleteBookRequest;
import com.api.agit.demo.contract.request.FilterByStatusRequest;
import com.api.agit.demo.contract.request.UpdateBookDataRequest;
import com.api.agit.demo.contract.request.UpdateBookStatusRequest;
import com.api.agit.demo.contract.response.CreateBookResponse;
import com.api.agit.demo.contract.response.DeleteBookResponse;
import com.api.agit.demo.contract.response.UpdateBookDataResponse;
import com.api.agit.demo.contract.response.UpdateBookStatusResponse;
import com.api.agit.demo.model.Book;
import com.api.agit.demo.repository.BookRepository;
import com.api.agit.demo.service.BookService;

@Service
public class BookServiceImpl implements BookService{

	private BookRepository bookRepository;

	@Autowired
	public void setBookRepository(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@Override
	public List<Book> getAllBooks() {
		return bookRepository.findAll();
	}

	@Override
	public List<Book> getDataByStatus(FilterByStatusRequest request) {
		return bookRepository.findByStatus(request.getStatus());
	}

	@Override
	public CreateBookResponse createBook(Book request) {
		CreateBookResponse response = new CreateBookResponse();
		try {
			Book data = bookRepository.findByIdBuku(request.getIdBuku());
			if(data!=null) {
				response.setData(request);
				response.setStatus("FAILED");
				response.setDeskripsi("ID BUKU SUDAH TERDAFTAR");
			}else {
				bookRepository.save(request);
				response.setData(request);
				response.setStatus("SUCCESS");
				response.setDeskripsi("BERHASIL MENAMBAHKAN DATA BUKU");
			}
		} catch (Exception e) {
			response.setData(request);
			response.setStatus("FAILED");
			response.setDeskripsi("GAGAL MENAMBAHKAN DATA BUKU");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public UpdateBookDataResponse updateDataBook(UpdateBookDataRequest request) {
		UpdateBookDataResponse response = new UpdateBookDataResponse();
		try {
			Book data = bookRepository.findByIdBuku(request.getIdBuku());
			if(data!=null) {
				data.setJudul(request.getJudul());
				data.setPengarang(request.getPengarang());
				data.setPenerbit(request.getPenerbit());
				data.setTglTerbit(request.getTglTerbit());
				data.setTebalBuku(request.getTebalBuku());
				bookRepository.save(data);
				response.setData(request);
				response.setStatus("SUCCESS");
				response.setDeskripsi("BERHASIL MENGUBAH DATA BUKU");
			}else {
				response.setData(request);
				response.setStatus("FAILED");
				response.setDeskripsi("DATA TIDAK DITEMUKAN");
			}
		} catch (Exception e) {
			response.setData(request);
			response.setStatus("FAILED");
			response.setDeskripsi("GAGAL MENGUBAH DATA BUKU");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public UpdateBookStatusResponse updateStatusBook(UpdateBookStatusRequest request) {
		UpdateBookStatusResponse response = new UpdateBookStatusResponse();
		try {
			Book data = bookRepository.findByIdBuku(request.getIdBuku());
			if(data!=null) {
				if(!data.getStatus().equalsIgnoreCase("0")) {
					response.setData(request);
					response.setStatus("FAILED");
					response.setDeskripsi("BUKU SEDANG TIDAK AVAILABLE");
					response.setStatusBuku("NOT AVAILABLE");
				}else {
					data.setStatus("1");
					data.setPeminjam(request.getPeminjam());
					data.setTglPinjam(request.getTglPinjam());
					data.setTglKembali(request.getTglKembali());
					bookRepository.save(data);
					response.setData(request);
					response.setStatus("SUCCESS");
					response.setDeskripsi("BERHASIL MENGUBAH STATUS BUKU");
					response.setStatusBuku("AVAILABLE");
				}
			}else {
				response.setData(request);
				response.setStatus("FAILED");
				response.setDeskripsi("DATA TIDAK DITEMUKAN");
			}
		} catch (Exception e) {
			response.setData(request);
			response.setStatus("FAILED");
			response.setDeskripsi("GAGAL MENGUBAH STATUS BUKU");
			response.setStatusBuku("FAILED");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public DeleteBookResponse delete(DeleteBookRequest request) {
		DeleteBookResponse response = new DeleteBookResponse();
		try {
			Book data = bookRepository.findByIdBuku(request.getIdBuku());
			if(data!=null) {
				bookRepository.delete(data);
				response.setStatus("SUCCESS");
				response.setDeskripsi("BERHASIL MENGHAPUS DATA BUKU");
			}else {
				response.setStatus("FAILED");
				response.setDeskripsi("DATA TIDAK DITEMUKAN");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
