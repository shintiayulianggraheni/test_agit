package com.api.agit.demo.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_buku")
public class Book {

	private Long id;
	private String idBuku;
	private String judul;
	private String pengarang;
	private String penerbit;
	private String tglTerbit;
	private String tebalBuku;
	private String status;
	private String peminjam;
	private String tglPinjam;
	private String tglKembali;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="id_buku")
	public String getIdBuku() {
		return idBuku;
	}
	public void setIdBuku(String idBuku) {
		this.idBuku = idBuku;
	}
	
	@Column(name="judul")
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	
	@Column(name="pengarang")
	public String getPengarang() {
		return pengarang;
	}
	public void setPengarang(String pengarang) {
		this.pengarang = pengarang;
	}
	
	@Column(name="penerbit")
	public String getPenerbit() {
		return penerbit;
	}
	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}
	
	@Column(name="tgl_terbit")
	public String getTglTerbit() {
		return tglTerbit;
	}
	public void setTglTerbit(String tglTerbit) {
		this.tglTerbit = tglTerbit;
	}
	
	@Column(name="tebal_buku")
	public String getTebalBuku() {
		return tebalBuku;
	}
	public void setTebalBuku(String tebalBuku) {
		this.tebalBuku = tebalBuku;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="peminjam")
	public String getPeminjam() {
		return peminjam;
	}
	public void setPeminjam(String peminjam) {
		this.peminjam = peminjam;
	}
	
	@Column(name="tgl_pinjam")
	public String getTglPinjam() {
		return tglPinjam;
	}
	public void setTglPinjam(String tglPinjam) {
		this.tglPinjam = tglPinjam;
	}
	
	@Column(name="tgl_kembali")
	public String getTglKembali() {
		return tglKembali;
	}
	public void setTglKembali(String tglKembali) {
		this.tglKembali = tglKembali;
	}
	
	
}
