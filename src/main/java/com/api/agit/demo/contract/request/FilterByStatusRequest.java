package com.api.agit.demo.contract.request;

import javax.validation.constraints.NotEmpty;

public class FilterByStatusRequest {

	@NotEmpty
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
