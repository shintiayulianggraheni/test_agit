package com.api.agit.demo.contract.request;

import javax.validation.constraints.NotEmpty;

public class DeleteBookRequest {

	@NotEmpty
	private String idBuku;

	public String getIdBuku() {
		return idBuku;
	}

	public void setIdBuku(String idBuku) {
		this.idBuku = idBuku;
	}
	
	
}
