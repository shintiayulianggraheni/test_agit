package com.api.agit.demo.contract.request;

import javax.validation.constraints.NotEmpty;

public class UpdateBookDataRequest {

	@NotEmpty
	private String idBuku;
	@NotEmpty
	private String judul;
	@NotEmpty
	private String pengarang;
	@NotEmpty
	private String penerbit;
	@NotEmpty
	private String tglTerbit;
	@NotEmpty
	private String tebalBuku;
	
	public String getIdBuku() {
		return idBuku;
	}
	public void setIdBuku(String idBuku) {
		this.idBuku = idBuku;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public String getPengarang() {
		return pengarang;
	}
	public void setPengarang(String pengarang) {
		this.pengarang = pengarang;
	}
	public String getPenerbit() {
		return penerbit;
	}
	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}
	public String getTglTerbit() {
		return tglTerbit;
	}
	public void setTglTerbit(String tglTerbit) {
		this.tglTerbit = tglTerbit;
	}
	public String getTebalBuku() {
		return tebalBuku;
	}
	public void setTebalBuku(String tebalBuku) {
		this.tebalBuku = tebalBuku;
	}
	
	
}
