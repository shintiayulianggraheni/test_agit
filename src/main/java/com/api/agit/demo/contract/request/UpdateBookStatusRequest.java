package com.api.agit.demo.contract.request;

import javax.validation.constraints.NotEmpty;

public class UpdateBookStatusRequest {

	@NotEmpty
	private String idBuku;
	@NotEmpty
	private String peminjam;
	@NotEmpty
	private String tglPinjam;
	@NotEmpty
	private String tglKembali;
	
	public String getIdBuku() {
		return idBuku;
	}
	public void setIdBuku(String idBuku) {
		this.idBuku = idBuku;
	}
	public String getPeminjam() {
		return peminjam;
	}
	public void setPeminjam(String peminjam) {
		this.peminjam = peminjam;
	}
	public String getTglPinjam() {
		return tglPinjam;
	}
	public void setTglPinjam(String tglPinjam) {
		this.tglPinjam = tglPinjam;
	}
	public String getTglKembali() {
		return tglKembali;
	}
	public void setTglKembali(String tglKembali) {
		this.tglKembali = tglKembali;
	}
	
	
	
}
