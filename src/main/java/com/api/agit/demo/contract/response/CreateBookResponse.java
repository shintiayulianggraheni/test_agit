package com.api.agit.demo.contract.response;

import com.api.agit.demo.model.Book;

public class CreateBookResponse {

	private String status;
	private String deskripsi;
	private Book data;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public Book getData() {
		return data;
	}
	public void setData(Book data) {
		this.data = data;
	}
	
	
}
