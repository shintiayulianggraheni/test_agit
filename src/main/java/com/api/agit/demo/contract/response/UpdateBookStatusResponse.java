package com.api.agit.demo.contract.response;

import com.api.agit.demo.contract.request.UpdateBookStatusRequest;

public class UpdateBookStatusResponse {

	private String status;
	private String deskripsi;
	private String statusBuku;
	private UpdateBookStatusRequest data;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getStatusBuku() {
		return statusBuku;
	}
	public void setStatusBuku(String statusBuku) {
		this.statusBuku = statusBuku;
	}
	public UpdateBookStatusRequest getData() {
		return data;
	}
	public void setData(UpdateBookStatusRequest data) {
		this.data = data;
	}
	
	
	
}
