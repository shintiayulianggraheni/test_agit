package com.api.agit.demo.contract.response;

import com.api.agit.demo.contract.request.UpdateBookDataRequest;

public class UpdateBookDataResponse {

	private String status;
	private String deskripsi;
	private UpdateBookDataRequest data;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public UpdateBookDataRequest getData() {
		return data;
	}
	public void setData(UpdateBookDataRequest data) {
		this.data = data;
	}
	
	
}
